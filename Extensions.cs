﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MerthBot.Plugins
{
    public static class Extensions
    {
        public static string Join<T>(this IEnumerable<T>? set, string separator)
            => set == null ? "" : string.Join(separator, set);

        public static IEnumerable<T?> SkipOrNull<T>(this IEnumerable<T> set, int count) where T : class
        {
            var currentCount = 0;
            using var enumerator = set.GetEnumerator();
            var canMove = true;
            while (currentCount < count)
            {
                if (canMove)
                    canMove = enumerator.MoveNext();
                else
                    yield return default;

                currentCount++;
            }

            while (enumerator.MoveNext())
                yield return enumerator.Current;
        }

        public static int? ToInt(this string? s)
            => s == null ? null : int.TryParse(s, out var i) ? i : (int?)null;

        public static TValue? SafeGet<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, TKey key) where TValue : class
            => dictionary.TryGetValue(key, out var ret) ? ret : default;

        public static string? ExtractString<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, TKey key) where TValue : class
            => dictionary.TryGetValue(key, out var ret) ? ret?.ToString() : default;

        public static void ForEach<T>(this IEnumerable<T>? set, Action<T> action)
        {
            if (set == null)
                return;

            foreach (var t in set)
                action(t);
        }

        public static TResult[] SelectArray<TSource, TResult>(this IEnumerable<TSource> set, Func<TSource, TResult> selector)
        {
            var ret = new List<TResult>();
            foreach (var t in set)
                ret.Add(selector(t));

            return ret.ToArray();
        }

        public static string JoinString<T>(this IEnumerable<T> set, Func<T, int, string> selector, string separator)
            => set.Select(selector).Join(separator);
    }
}
