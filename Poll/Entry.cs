﻿namespace MerthBot.Plugins
{
    internal class Entry
    {
        public string Value { get; }

        public int Total { get; set; }

        public ulong MessageNumber { get; set; }

        public bool Active { get; set; } = true;

        public string[]? UserNames { get; set; }

        public Entry(string value)
            => Value = value;

        private string BaseGetString
            => $"{Value}" + (Active ? string.Empty : $" - {Total}") + (Total == 0 ? string.Empty : $" ({UserNames.Join(", ")})");

        public override string ToString()
            => $"• {BaseGetString}";

        public string ToNumberedString(int number)
            => $"{number}) {BaseGetString}";
    }
}