﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MerthBot.Plugins
{
    internal class Poll
    {
        private const string DefaultTitle = "***Vote now by reacting!***";
        private const string PollOverTitle = "***Voting Ended!***";

        private const string DefaultMessage = "*The entries are:*";
        private const string PollOverMessage = "*The results are:*";

        public int Number { get; }

        public string? Title { get; }

        public string PaddedTitle
            => Title == null ? string.Empty : Title + " ";

        public string? Message { get; }

        public ulong MessageNumber { get; set; }

        public bool Active { get; set; } = true;

        private readonly List<Entry> unclaimedEntries = new List<Entry>();
        private readonly List<Entry> entries = new List<Entry>();

        public IReadOnlyList<Entry> Entries
            => entries.ToArray();

        public IEnumerable<string> AllUserNames
            => entries.Where(e => e.UserNames != null).SelectMany(e => e.UserNames!);

        public IEnumerable<string> UniqueUserNames
            => AllUserNames.Distinct();

        public IReadOnlyList<string> UniqueUserNamesSorted
            => UniqueUserNames.OrderBy(n => n).ToList();

        public int TotalUniqueVotes
            => AllUserNames.Count();


        public Poll(int number, IEnumerable<string> initialEntries, string? title = null, string? message = null)
        {
            (Number, Title, Message) = (number, title, message);
            unclaimedEntries.AddRange(initialEntries.Take(5).Select(CreateEntry));
        }

        private Entry CreateEntry(string value)
            => new Entry(value);

        public async Task ClaimAll(Func<Entry, Task<ulong>> action)
        {
            foreach (var entry in unclaimedEntries)
            {
                entry.MessageNumber = await action(entry);
                entries.Add(entry);
            }
            unclaimedEntries.Clear();
        }

        public override string ToString()
            => $"#{Number} - {PaddedTitle}{(Active ? DefaultTitle : PollOverTitle)}\n{Message ?? (Active ? DefaultMessage : PollOverMessage)}";
    }
}