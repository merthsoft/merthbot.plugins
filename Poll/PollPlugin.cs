﻿using Discord;
using Discord.WebSocket;
using MerthBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerthBot.Plugins
{
    public static partial class PollPlugin
    {
        private static Dictionary<int, Poll> Polls { get; } = new Dictionary<int, Poll>();

        private static int PollNumber { get; set; } = 0;

        public static async Task MakePoll(IMessage message, ICommand command, string[] splitMessage, Bot bot)
        {
            var commandString = splitMessage.Skip(1).Join(" ");

            Poll? poll = null;
            if (commandString.StartsWith("\""))
            {
                poll = HandleQuotedInput(PollNumber++, commandString);
            }
            else
            {
                var initialEntries = commandString.Split('|', StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s));
                if (initialEntries.Count() >= 2)
                {
                    poll = new Poll(PollNumber++, initialEntries);
                }
            }

            if (poll == null)
            {
                await bot.SendMessage(message.Channel, command.Description);
                return;
            }
            Polls[poll.Number] = poll;

            var guild = (message.Channel as SocketGuildChannel)?.Guild;
            var emote = guild?.Emotes.FirstOrDefault(e => e.Name == "moon6");

            var pollMessage = await bot.SendMessage(message.Channel, poll.ToString());
            poll.MessageNumber = pollMessage.Id;
            await poll.ClaimAll(async(entry) =>
            {
                var entryMessage = await bot.SendMessage(message.Channel, entry.ToString());
                if (emote != null)
                    _ = entryMessage.AddReactionAsync(emote);
                return entryMessage.Id;
            });
        }

        public static async Task EndPoll(IMessage message, ICommand command, string[] splitMessage, Bot bot)
        {
            var pollNumber = splitMessage.SkipOrNull(1).FirstOrDefault().ToInt();
            if (pollNumber == null)
            {
                await bot.SendMessage(message.Channel, command.Description);
                return;
            }

            var poll = Polls.SafeGet(pollNumber.Value);
            if (poll == null)
            {
                await bot.SendMessage(message.Channel, $"Poll {pollNumber.Value} not found.");
                return;
            }

            if (!(await message.Channel.GetMessageAsync(poll.MessageNumber) is IUserMessage pollMessage))
            {
                await bot.SendMessage(message.Channel, $"Poll {pollNumber.Value} not found on this channel.");
                return;
            }

            poll.Active = false;
            await pollMessage.ModifyAsync(x => x.Content = poll.ToString());
            var maxTotal = int.MinValue;
            foreach (var entry in poll.Entries)
            {
                entry.Active = false;

                var entryMessage = await message.Channel.GetMessageAsync(entry.MessageNumber) as IUserMessage;
                _ = entryMessage ?? throw new NullReferenceException("Could not get user message from channel.");

                var allEmotes = entryMessage.Reactions.Keys;
                var uniqueNickNames = new HashSet<string>();
                var channel = message.Channel as SocketGuildChannel;
                _ = channel ?? throw new NullReferenceException("Could not get channel from message.");

                foreach (var emote in allEmotes)
                {
                    await foreach (var users in entryMessage.GetReactionUsersAsync(emote, 100))
                    {
                        users.Select(u => channel.GetUser(u.Id)).Where(u => !u.IsBot).ForEach(u => uniqueNickNames.Add(u.Nickname ?? u.Username));
                    }
                }
                entry.Total = uniqueNickNames.Count;
                entry.UserNames = uniqueNickNames.OrderBy(s => s).SelectArray(s => s.Trim());

                if (entry.Total > maxTotal)
                    maxTotal = entry.Total;

                await entryMessage.ModifyAsync(x => x.Content = entry.ToString());
            }

            var finalMessage = new StringBuilder()
                .AppendLine($"{poll.PaddedTitle}The results are:")
                .AppendLine($"{poll.Entries.OrderByDescending(e => e.Total).JoinString((e, i) => e.ToNumberedString(i + 1), "\n")}")
                .AppendLine($"Total votes: {poll.TotalUniqueVotes} ({poll.UniqueUserNamesSorted.Join(", ")})");

            await bot.SendMessage(message.Channel, finalMessage.ToString());
        }

        private static Poll? HandleQuotedInput(int pollNumber, string commandString)
        {
            var split = commandString.Split(new[] { "\" " }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 2)
                return null;

            var initialEntries = split.Skip(1).Join("\" ").Split('|', StringSplitOptions.RemoveEmptyEntries);
            if (initialEntries.Length < 2)
                return null;

            return new Poll(pollNumber, initialEntries, title: split[0].Substring(1));
        }
    }
}
