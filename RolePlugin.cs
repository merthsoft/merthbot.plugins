﻿using Discord;
using Discord.WebSocket;
using MerthBot.Interfaces;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace MerthBot.Plugins
{
    internal class RolePlugin
    {
        public static async Task AssignRoll(IMessage message, ICommand command, string[] splitMessage, Bot bot)
        {
            var allowedRolesString = command.ExtraData.ExtractString("allowedRoles")
                ?? throw new ArgumentNullException("allowedRoles not configured on command.");

            var allowedRoles = JsonSerializer.Deserialize<ulong[]>(allowedRolesString)
                ?? throw new Exception("allowedRoles not configured correctly on command. Expecting a json array of numbers.");

            var commandString = splitMessage.Skip(1).Join(" ");
            if (string.IsNullOrWhiteSpace(commandString))
            {
                await bot.SendMessage(message.Channel, command.Description);
                return;
            }

            var author = (message.Author as SocketGuildUser);
            _ = author ?? throw new NullReferenceException("Could not get author from message.");

            var roles = author.Guild.Roles;
            SocketRole? role = null;
            if (ulong.TryParse(commandString, out var roleId))
                role = roles.FirstOrDefault(r => r.Id == roleId);
            else
                role = roles.FirstOrDefault(r => r.Name == commandString);

            if (role == null)
            {
                await bot.SendMessage(message.Channel, $"Could not find role \"{commandString}\".");
                return;
            }

            if (!allowedRoles.Contains(role.Id))
            {
                await bot.SendMessage(message.Channel, $"Role \"{role.Name}\" is not allowed.");
                return;
            }
            
            await author.AddRoleAsync(role);
            await bot.SendMessage(message.Channel, $"Role assigned.");
        }
    }
}
